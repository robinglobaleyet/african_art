-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2019 at 06:50 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `african_art`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `slug` varchar(500) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `third_category_id` int(11) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `product_condition` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `zip_code` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `is_promoted` int(11) DEFAULT 0,
  `promote_start_date` timestamp NULL DEFAULT NULL,
  `promote_end_date` timestamp NULL DEFAULT NULL,
  `promote_plan` varchar(100) DEFAULT NULL,
  `promote_day` int(11) DEFAULT NULL,
  `visibility` int(11) NOT NULL DEFAULT 1,
  `rating` varchar(50) DEFAULT '0',
  `hit` int(11) DEFAULT 0,
  `external_link` varchar(1000) DEFAULT NULL,
  `quantity` int(11) DEFAULT 1,
  `shipping_time` varchar(100) DEFAULT '2_3_business_days',
  `shipping_cost_type` varchar(100) DEFAULT NULL,
  `shipping_cost` bigint(20) DEFAULT 0,
  `height` varchar(100) NOT NULL,
  `width` varchar(100) NOT NULL,
  `is_sold` int(11) DEFAULT 0,
  `is_deleted` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `slug`, `category_id`, `subcategory_id`, `third_category_id`, `price`, `currency`, `description`, `product_condition`, `country_id`, `state_id`, `address`, `zip_code`, `user_id`, `status`, `is_promoted`, `promote_start_date`, `promote_end_date`, `promote_plan`, `promote_day`, `visibility`, `rating`, `hit`, `external_link`, `quantity`, `shipping_time`, `shipping_cost_type`, `shipping_cost`, `height`, `width`, `is_sold`, `is_deleted`, `created_at`) VALUES
(1, 'Africa Art', 'africa-art-1', 1, 0, 0, 10000, 'USD', '<p>&nbsp;details about art</p>\r\n', 'new', 101, 19, 'attingal', '695101', 2, 1, 0, '2019-08-28 02:45:32', '2019-08-28 02:45:32', 'none', 0, 1, '0', 3, '', 10, '2_3_business_days', 'shipping_buyer_pays', 1000, '0', '0', 0, 0, '2019-08-28 02:45:32'),
(2, 'Africa Design', 'africa-design-2', 1, 0, 0, 20000, 'USD', '<p>description</p>\r\n', 'very_good', 101, 19, '', '', 2, 1, 0, '2019-08-28 02:55:51', '2019-08-28 02:55:51', 'none', 0, 1, '0', 2, '', 10, '1_business_day', 'free_shipping', 0, '0', '0', 0, 0, '2019-08-28 02:55:51'),
(3, 'craft work', 'craft-work-3', 3, 0, 0, 5000, 'USD', '<p>paper works</p>\r\n', 'very_good', 0, 0, '', '', 4, 1, 0, '2019-08-29 02:43:03', '2019-08-29 02:43:03', 'none', 0, 1, '0', 1, '', 50, '2_3_business_days', 'shipping_included', 0, '0', '0', 0, 1, '2019-08-29 02:43:03'),
(4, 'craft work', 'craft-work-4', 3, 0, 0, 5000, 'USD', '<p>paper works</p>\r\n', 'new_with_tags', 0, 0, '', '', 4, 1, 0, '2019-08-29 02:52:59', '2019-08-29 02:52:59', 'none', 0, 1, '0', 1, '', 5, '1_business_day', 'free_shipping', 0, '0', '0', 0, 1, '2019-08-29 02:52:59'),
(5, 'paintings', 'paintings-5', 1, 0, 0, 10000, 'USD', '<p>ancient painting&nbsp;</p>\r\n', 'very_good', 0, 0, '', '', 5, 1, 0, '2019-08-29 03:59:12', '2019-08-29 03:59:12', 'none', 0, 1, '0', 1, '', 5, '2_3_business_days', 'free_shipping', 0, '0', '0', 0, 1, '2019-08-29 03:59:12'),
(6, 'craft work', 'craft-work-6', 3, 0, 0, 5000, 'USD', '<p>craft&nbsp; with paper</p>\r\n', 'new_with_tags', 0, 0, '', '', 4, 1, 0, '2019-08-29 04:33:52', '2019-08-29 04:33:52', 'none', 0, 1, '0', 1, '', 10, '1_business_day', 'free_shipping', 0, '0', '0', 0, 0, '2019-08-29 04:33:52'),
(7, 'paper craft', 'paper-craft-7', 3, 0, 0, 5000, 'USD', '<p>paper craft,affordable</p>\r\n', 'new', 0, 0, '', '', 4, 1, 0, '2019-08-29 04:34:46', '2019-08-29 04:34:46', 'none', 0, 1, '0', 1, '', 5, '1_business_day', 'shipping_included', 0, '0', '0', 0, 0, '2019-08-29 04:34:46'),
(8, 'african culture', 'african-culture-8', 1, 0, 0, 10000, 'USD', '<p>African paintings</p>\r\n', 'new_with_tags', 0, 0, '', '', 4, 1, 0, '2019-08-29 04:38:12', '2019-08-29 04:38:12', 'none', 0, 1, '0', 1, '', 5, '4_7_business_days', 'shipping_included', 0, '0', '0', 0, 0, '2019-08-29 04:38:12'),
(9, '50cm', '50cm-9', 2, 0, 0, 10000, 'USD', '<p>very ancient</p>\r\n', 'new_with_tags', 0, 0, '', '', 1, 0, 0, '2019-08-29 06:42:36', '2019-08-29 06:42:36', 'none', 0, 1, '0', 1, '', 1, '2_3_business_days', 'shipping_included', 0, '0', '0', 0, 0, '2019-08-29 06:42:36'),
(10, 'Independence day', 'independence-day-10', 2, 0, 0, 10000, 'USD', '<p>sdfgdfg</p>\r\n', 'new', 0, 0, '', '', 1, 0, 0, '2019-08-30 00:11:31', '2019-08-30 00:11:31', 'none', 0, 1, '0', 0, '', 2, '1_business_day', 'free_shipping', 0, '222', '122', 0, 0, '2019-08-30 00:11:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
