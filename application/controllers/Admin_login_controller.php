<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login_controller extends Admin_Core_Controller
{
    public function __construct()
    {
        parent::__construct();
        //check user
       /* if (!is_admin()) {
            redirect(base_url());
        }
		*/
    }

    public function index()
    {
        $data['title'] = trans("admin_panel");
        
       
        //$data['latest_transactions'] = $this->transaction_model->get_transactions_limited(15);
        //$data['latest_promoted_transactions'] = $this->transaction_model->get_promoted_transactions_limited(15);

        $this->load->view('admin/login', $data);
    }
	
	public function admin_login()
	{
		
		// email  password
		//print_r($_POST);
		$result=$this->auth_model->adminlogin();
		if($result)
		{
			redirect('admin');
        }
        else
        {
			redirect('admin/login');
        }
		
		

       /* $data = $this->input_values();
        $user = $this->get_user_by_email($data['email']);

        if (!empty($user)) {
            //check password
            if (!$this->bcrypt->check_password($data['password'], $user->password)) {
                $this->session->set_flashdata('error', trans("login_error"));
                return false;
            }
            if ($user->banned == 1) {
                $this->session->set_flashdata('error', trans("msg_ban_error"));
                return false;
            }
            //set user data
            $user_data = array(
                'modesy_sess_user_id' => $user->id,
                'modesy_sess_user_email' => $user->email,
                'modesy_sess_user_role' => $user->role,
                'modesy_sess_logged_in' => true,
                'modesy_sess_app_key' => $this->config->item('app_key'),
            );
            $this->session->set_userdata($user_data);
            return true;
        } else {
            $this->session->set_flashdata('error', trans("login_error"));
            return false;
        }
		*/
		
	}

   
}
