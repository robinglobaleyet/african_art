<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Wrapper -->
<div id="wrapper" class="index-wrapper">
    <div class="container container-slider">
        <?php if (!empty($slider_items) && $general_settings->index_slider == 1): ?>
            <div class="section section-slider">
                <!-- main slider -->
                <?php $this->load->view("partials/_main_slider"); ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="container">
        <div class="row">

            <h1 class="index-title"><?php echo html_escape($settings->site_title); ?></h1>
            <?php if ($featured_category_count > 0 && $general_settings->index_categories == 1): ?>
                <div class="col-12 section section-categories">
                    <!-- featured categories -->
                    <?php $this->load->view("partials/_featured_categories"); ?>
                </div>
            <?php endif; ?>
            <div class="col-12">
                <div class="row-custom row-bn">
                    <!--Include banner-->
                    <?php $this->load->view("partials/_ad_spaces", ["ad_space" => "index_1", "class" => ""]); ?>
                </div>
            </div>
            <?php if ($general_settings->index_promoted_products == 1 && $promoted_products_enabled == 1 && !empty($promoted_products)): ?>
                <div class="col-12 section section-promoted">
                    <!-- promoted products -->
                    <?php $this->load->view("product/_promoted_products"); ?>
                </div>
            <?php endif; ?>
            <?php if ($general_settings->index_latest_products == 1 && !empty($latest_products)): ?>
                <div class="col-12 section section-latest-products">
                    <h3 class="title"><?php echo trans("latest_products"); ?></h3>
                    <p class="title-exp"><?php echo trans("latest_products_exp"); ?></p>
					
						<div class="row">
						<div class="col-sm-12 col-md-3 col-lg-3">
						   <form method="GET" action="<?php echo lang_base_url(); ?>products" name="product_search"  id="product_search">
							  <table class="table table-bordered">
								 <th>Filters</th>
								 <tr>
									<td><input type="text" name="search" class="form-control" placeholder="Name"></td>
								 </tr>
								 <tr>
									<td><input type="text" name="region"  class="form-control" placeholder="Region"></td>
								 </tr>
								 <tr>
									<td><input type="text" name="size" class="form-control"  placeholder="Size"></td>
								 </tr>
								 <tr>
									<td>
									   Price &nbsp;
									   <p type="text" id="amount"   readonly style="border:0; color:#f6931f; font-weight:bold;"></p>           
									   <div id="slider-range"></div>
									</td>
								 </tr>
								 <tr>
									<td>
									   <input type="hidden" name="p_min" id="price_min" value="0" >
									   <input type="hidden" name="p_max" id="price_max" value="1000">
									   <button type="submit" class="btn btn-warning art_search">Search</button>
									</td>
								 </tr>
							  </table>
						   </form>
						</div>

                        <!--print products-->
                        <?php foreach ($latest_products as $product): ?>
                            <div class="col-sm-12 col-md-4 col-lg-3">
                                <?php $this->load->view('product/_product_item', ['product' => $product, 'promoted_badge' => false]); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="row-custom text-center">
                        <a href="<?php echo lang_base_url() . "products"; ?>" class="link-see-more"><span><?php echo trans("see_more"); ?>&nbsp;</span><i class="icon-arrow-right"></i></a>
                    </div>
                </div>
<!-- globaleyet-->
<?php
foreach($get_featured_artist as $featured_artist)
{

$artist_products=$this->product_model->get_featured_product_artist($featured_artist->id);
    ?>
 <div class="col-12 section section-featured-artist">

                    <h3 class="title text-left"><?php echo "featured-artist"; ?></h3>
                    <p class="title-exp text-left"><?php echo $featured_artist->username; ?></p>
                    <div class="row">
                        <!--print products-->
                        <?php foreach ($artist_products as $product): ?>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <?php $this->load->view('product/_product_item', ['product' => $product, 'promoted_badge' => false]); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="row-custom text-center">
                        <a href="<?php echo lang_base_url() . "products"; ?>" class="link-see-more"><span><?php echo trans("see_more"); ?>&nbsp;</span><i class="icon-arrow-right"></i></a>
                    </div>
                </div>
<?php
}

?>
<!--globaleyet-->



            <?php endif; ?>



            <div class="col-12">
                <div class="row-custom row-bn">
                    <!--Include banner-->
                    <?php $this->load->view("partials/_ad_spaces", ["ad_space" => "index_2", "class" => ""]); ?>
                </div>
            </div>
            <?php if ($general_settings->index_blog_slider == 1 && !empty($blog_slider_posts)): ?>
                <div class="col-12 section section-blog m-0">
                    <h3 class="title"><?php echo trans("latest_blog_posts"); ?></h3>
                    <p class="title-exp"><?php echo trans("latest_blog_posts_exp"); ?></p>
                    <div class="row-custom">
                        <!-- main slider -->
                        <?php $this->load->view("blog/_blog_slider", ['blog_slider_posts' => $blog_slider_posts]); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- Wrapper End-->


<script>
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 1000,
            values: [ 1, 1000 ],
            slide: function( event, ui ) {
                $( "#amount" ).html( "€" + ui.values[ 0 ] + " - €" + ui.values[ 1 ] );
				$( "#price_min" ).val(ui.values[ 0 ]);
				$( "#price_max" ).val(ui.values[ 1 ]);
				
            }
        });
        $( "#amount" ).html( "€" + $( "#slider-range" ).slider( "values", 0 ) +
            " - €" + $( "#slider-range" ).slider( "values", 1 ) );
    } );
    </script>
	<style>
	.art_search
	{
		width:100%;
		padding:15px;
	}
	</style>
