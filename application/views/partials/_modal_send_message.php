<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Send Message Modal -->
<?php if (auth_check()): ?>
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-send-message" role="document">

            <div class="modal-content">
                <!-- form start -->
                <form id="form_send_message" novalidate="novalidate">
                    <input type="hidden" name="sender_id" value="<?php echo user()->id; ?>">
                    <input type="hidden" name="receiver_id" id="message_receiver_id" value="<?php echo $user->id; ?>">
                    <input type="hidden" id="message_send_em" value="<?php echo $user->send_email_new_message; ?>">

                    <div class="modal-header">
                        <h4 class="title"><?php echo trans("send_message"); ?></h4>
                        <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div id="send-message-result"></div>
                                <div class="form-group m-b-sm-0">
                                    <div class="row justify-content-center m-0">
                                        <div class="user-contact-modal">
                                            <img src="<?php echo get_user_avatar($user); ?>" alt="<?php echo html_escape($user->username); ?>">
                                            <p><?php echo html_escape($user->username); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?php echo trans("subject"); ?></label>
                                    <input type="text" name="subject" id="message_subject" value="<?php echo (!empty($subject)) ? html_escape($subject) : ''; ?>" class="form-control form-input" placeholder="<?php echo trans("subject"); ?>" required>
                                </div>
                                <div class="form-group m-b-sm-0">
                                    <label class="control-label"><?php echo trans("message"); ?></label>
                                    <textarea name="message" id="message_text" class="form-control form-textarea" placeholder="<?php echo trans("write_a_message"); ?>" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-red" data-dismiss="modal"><i class="icon-times"></i>&nbsp;<?php echo trans("close"); ?></button>
                        <button type="submit" class="btn btn-md btn-custom"><i class="icon-send"></i>&nbsp;<?php echo trans("send"); ?></button>
                    </div>
                </form>
                <!-- form end -->
            </div>

        </div>
    </div>
<?php endif; ?>




<?php if (auth_check()): ?>
    <div class="modal fade" id="commissionModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-send-message" role="document">

            <div class="modal-content">
                <!-- form start -->
                <form id="form_commision_artist" novalidate="novalidate">
                    <input type="hidden" name="commission_sender_id" value="<?php echo user()->id; ?>">
                    <input type="hidden" name="receiver_id" id="commission_receiver_id" value="<?php echo $user->id; ?>">
                    <input type="hidden" id="commission_message_send_em" value="<?php echo $user->send_email_new_message; ?>">
                     <input type="hidden" name="commision_product_id" id="commision_product_id" value="<?php echo $product->id; ?>">
                    <div class="modal-header">
                        <h4 class="title"> Commision This Artist<?php //echo trans("send_message"); ?></h4>
                        <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div id="commission-message-result"></div>

                                <div class="form-group">
                                    <input type="input" name="custom_height"  id="custom_height" placeholder="<?php echo trans("customheight"); ?>"  class="form-control commission_input" required >
                                </div>    
                           
                                   <div class="form-group">
                                    <input type="input" name="custom_width" id="custom_width" placeholder="<?php echo trans("customwidth"); ?>"  class="form-control commission_input" required >
                                </div>   


                                     <div class="form-group">
                                    <input type="date" name="delivery_date" id="delivery_date"  placeholder="<?php echo trans("customheight"); ?>"  class="form-control commission_input" required >
                                </div>   
                                

                                <div class="form-group m-b-sm-0">
                                   
                                    <textarea name="commission_text" id="commission_text" class="form-control form-textarea" placeholder="Enter the Description or Specification " required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-red" data-dismiss="modal"><i class="icon-times"></i>&nbsp;<?php echo trans("close"); ?></button>
                        <button type="submit" class="btn btn-md btn-custom"><i class="icon-send"></i>&nbsp;Proceed</button>
                    </div>
                </form>
                <!-- form end -->
            </div>

        </div>
    </div>
<?php endif; ?>





<?php if (auth_check()): ?>
    <div class="modal fade" id="commission_priceModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-send-message" role="document">

            <div class="modal-content">
                <!-- form start -->
                <form id="form_commision_price" novalidate="novalidate">
                    <input type="hidden" name="commission_sender_id" value="<?php echo user()->id; ?>">
                    <input type="hidden" name="receiver_id" id="user_receiver_id" value="">
                    <input type="hidden" id="commission_message_send_em" value="1">
                     <input type="hidden" name="commision_product_id" id="commis_product_id" value="">
                    <input type="hidden" name="commision_id" id="commision_id" value="">
                     
                    <div class="modal-header">
                        <h4 class="title" id="commission_title"> </h4>
                        <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div id="commissionprice-message-result"></div>

                                <div class="form-group">
                                     <input type="hidden" name="product_name"  id="product_name">
                                    <input type="input" name="custom_price"  id="custom_price" placeholder="Price"  class="form-control commission_input" required >
                                </div>    
                           
                          
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-red" data-dismiss="modal"><i class="icon-times"></i>&nbsp;<?php echo trans("close"); ?></button>
                        <button type="submit" class="btn btn-md btn-custom"><i class="icon-send"></i>&nbsp;Proceed</button>
                    </div>
                </form>
                <!-- form end -->
            </div>

        </div>
    </div>
<?php endif; ?>

