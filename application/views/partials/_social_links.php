<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<ul>
    <?php if (!empty($settings->facebook_url)): ?>
        <li><a href="<?php echo html_escape($settings->facebook_url); ?>"><i style="background:#3b5998" class="sm-icon icon-facebook"></i></a></li>
    <?php endif; ?>
    <?php if (!empty($settings->twitter_url)): ?>
        <li><a href="<?php echo html_escape($settings->twitter_url); ?>"><i style="background:#1DA1F2" class="sm-icon icon-twitter"></i></a></li>
    <?php endif; ?>
    <?php if (!empty($settings->instagram_url)): ?>
        <li><a href="<?php echo html_escape($settings->instagram_url); ?>"><i style="background:#833AB4;"  class="sm-icon icon-instagram"></i></a></li>
    <?php endif; ?>
 

    <?php if (!empty($settings->linkedin_url)): ?>
        <li><a href="<?php echo html_escape($settings->linkedin_url); ?>"><i style="background:#0e76a8" class="sm-icon icon-linkedin"></i></a></li>
    <?php endif; ?>

  
    <?php if (!empty($settings->youtube_url)): ?>
        <li><a href="<?php echo html_escape($settings->youtube_url); ?>"><i style="background:#c4302b" class="sm-icon icon-youtube"></i></a></li>
    <?php endif; ?>
   
</ul>