<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div id="main-slider" class="owl-carousel main-slider">

	
    <?php foreach ($slider_items as $item): ?>
	
        <div class="item">
		<div class="row">
		<div class="col-md-6 no-padding">
		
            <a href="<?php echo $item->link; ?>">
                <img src="<?php echo get_slider_image_url($item); ?>"  style="height:400px"  class="owl-image" alt="slider">
            </a>
			
		</div>
		
		<div class="col-md-6 no-padding">
		<div class="row">
		<div class="col-md-12 no-padding" style="height:200px;">
		<img src=" <?php echo base_url() ?><?php echo $slider_top[0]->image?>" style="height:190px">
		</div>
		</div>
		<div class="row">
		<div class="col-md-6 no-padding" style="height:200px;">
		<img src=" <?php echo base_url() ?><?php echo $slider_left[0]->image?>" style="height:200px">
		</div>
		
		<div class="col-md-6 no-padding" style="height:200px; ">
		<img src="<?php echo base_url() ?><?php echo $slider_right[0]->image?>" style="height:200px">
		</div>
		</div>
		
        </div>
		</div>
		
		
        </div>
		
	


	
    <?php endforeach; ?>
</div>

