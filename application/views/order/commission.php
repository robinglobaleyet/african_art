<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Wrapper -->
<div id="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="nav-breadcrumb" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo lang_base_url(); ?>"><?php echo trans("home"); ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $title; ?></li>
                    </ol>
                </nav>

                <h1 class="page-title"><?php echo $title; ?></h1>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-3">
                <div class="row-custom">
                    <!-- load profile nav -->
                    <?php $this->load->view("order/_order_tabs"); ?>
                </div>
            </div>

            <div class="col-sm-12 col-md-9">
                <div class="row-custom">
                    <div class="profile-tab-content">

                        <!-- include message block -->
                        <?php $this->load->view('partials/_messages'); ?>
                        <div class="table-responsive">
                            <table class="table table-orders">
                                <thead>
                                <tr>
                                    <th scope="col">Art</th>

                                <?php
                                  
                                     if($_SESSION['modesy_sess_user_role']=='buyer')
                                     {
                                        ?>
                                        <th scope="col">Artist</th>
                                        <?php
                                     }  
                                     else
                                     {
                                        ?>
                                        <th scope="col">Customer</th>
                                        <?php

                                     }

                                     ?>
                                    


                                    <th scope="col"> Height</th>
                                    <th scope="col"> Width</th>
                                    <th scope="col">Delivery</th>
                                     <th scope="col">Fixed Price</th>
                                     <th scope="col">price</th>
                                     <th scope="col">Requested</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($commissions)): ?>
                                    <?php foreach ($commissions as $commission): ?>
                                        <tr>
                                            <td>#<?php echo $commission->title; ?></td>
                                              <td>
                                            <?php echo $commission->username?>
                                            </td>
                                             <td>
                                                <?php echo $commission->custom_height?>
                                            </td>

                                            <td>
                                                <?php echo $commission->custom_width?>
                                            </td>

                                            <td>
                                                <?php echo $commission->delivery_date?>
                                            </td>
                                            <td><?php echo print_price($commission->fixedprice, $order->currency); ?></td>
                                          
                                           <td><?php echo $commission->newprice; ?></td>
                                            <td><?php echo date("Y-m-d / h:i", strtotime($commission->requested)); ?></td>
                                            <td>
                                                <!--<a href="<?php echo lang_base_url(); ?>order/<?php echo $order->order_number; ?>" class="btn btn-sm btn-custom">Set price</a> -->
                                                <?php
                                                   if($_SESSION['modesy_sess_user_role']=='seller')
                                                     {
                                                         if($commission->cstatus==1)
                                                        {
                                                            echo "Approved";
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                           <button  onmouseover="commission_price(<?php echo $commission->cid?>,<?php echo $commission->pid?>,'<?php echo $commission->title; ?>',<?php echo $commission->uid; ?>)" class="btn btn-commission" data-toggle="modal" data-target="#commission_priceModal">Update Price</button>
                                                        <?php
                                                    }
                                                     }  
                                                     else  if($_SESSION['modesy_sess_user_role']=='buyer')
                                                     {
                                                        if($commission->cstatus==1)
                                                        {
                                                        ?>
                                                        <button  onmouseover="commission_price(<?php echo $commission->cid?>,<?php echo $commission->pid?>,'<?php echo $commission->title; ?>',<?php echo $commission->uid; ?>)" class="btn btn-success" data-toggle="modal" data-target="#commission_priceModal">Order Now</button>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                           ?>
                                                            <button class="btn btn-commission" class="badge"> Not Approved</button>
                                                            <?php
                                                        }

                                                     }

                                                       else
                                                        {
                                                              if($commission->cstatus==2)
                                                        {

                                                           ?>
                                                            <button onclick="commission_price_accept(<?php echo $commission->cid?>,<?php echo $commission->pid?>,'<?php echo $commission->title; ?>',<?php echo $commission->uid; ?>)" class="btn btn-commission" class="badge"> Accept</button>
                                                            <?php
                                                        }
                                                        if($commission->cstatus==1)
                                                        {
                                                            echo "Approved";
                                                        }
                                                        }

                                                     ?>

                                             
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>


                        <?php if (empty($commissions)): ?>
                            <p class="text-center">
                                <?php echo trans("no_records_found"); ?>
                            </p>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row-custom m-t-15">
                    <div class="float-right">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Wrapper End-->

<?php $this->load->view("partials/_modal_send_message", ["subject" => 'Set Price']); ?>

<script>
    function commission_price(cid,pid,pname,receiver_id)
    {
        //alert('Development');
      document.getElementById("commissionprice-message-result").innerHTML ='';
      $('#commission_title').html(pname);
      $('#product_name').val(pname);
      $('#commis_product_id').val(pid);
      $('#commision_id').val(cid);
      //alert(receiver_id);
      $('#user_receiver_id').val(receiver_id);

      
      

    }
</script>    